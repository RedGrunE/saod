#pragma once
#include <string.h>
#include <iostream>
class Str
{
	_str* m_pStr;
public:
	Str() {
		m_pStr = new _str;
		m_pStr->m_pszData = new char[1]{ 0 };
	}		
	Str(const char * p) {		
		m_pStr = new _str(p);
	}
	operator const char *()const {
		return m_pStr->m_pszData;
	}

	char operator [](int i) const {
		if (i >= 0 && i < len())
		{
			return m_pStr->m_pszData[i];
		}		
	}
	char& operator [](int i) {
		if (i >= 0 && i < len())
		{
			return m_pStr->m_pszData[i];
		}		
	}
	Str(const Str &s)
	{
		m_pStr = s.m_pStr; 	
		m_pStr->AddRef(); 
	}
	~Str() {
		m_pStr->Release(); 	
	}

	int len() const {
		return (m_pStr->m_pszData) ?
			strlen(m_pStr->m_pszData) : 0;
	}

	Str & operator = (const Str &s) {
		if (this != &s) {
			s.m_pStr->AddRef();
			m_pStr->Release();
			m_pStr = s.m_pStr;
		}
		return *this;
	}

	Str & operator += (const Str &s) {
		int length = len() + s.len();
		if (length != 0) {
			_str *pstrTmp = new _str; 	
			pstrTmp->m_pszData = new char[length + 1];
			if (m_pStr->m_pszData)
				strcpy_s(pstrTmp->m_pszData, length + 1, m_pStr->m_pszData);
			else
				*(pstrTmp->m_pszData) = 0;
			if (s.m_pStr->m_pszData)
				strcat_s(pstrTmp->m_pszData, length + 1, s.m_pStr->m_pszData);

			m_pStr->Release();
			m_pStr = pstrTmp;
		}
		return *this;
	}

};

