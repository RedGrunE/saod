#include "pch.h"
#include "ClassComplex.h"
#include "iostream"

using namespace std;


void ClassComplex::Add_Data(double real, double mnim)
{
	this->Real = real;
	this->Mnim = mnim;
}

void ClassComplex::Show()
{
	cout << '(' << this->Real << " + " << this->Mnim << "i)";
}


ClassComplex::ClassComplex()
{
}


ClassComplex::~ClassComplex()
{
}
