#pragma once
class ClassComplex
{
public:
	ClassComplex();
	~ClassComplex();
	double Real;
	double Mnim;
	void Add_Data(double real, double mnim);
	void Show();
	
	friend const ClassComplex operator+ (ClassComplex const x, ClassComplex const y) {
		ClassComplex result;
		result.Real = x.Real + y.Real;
		result.Mnim = x.Mnim + y.Mnim;
		return result;
	}

	friend const ClassComplex operator- (ClassComplex const x, ClassComplex const y) {
		ClassComplex result;
		result.Real = x.Real - y.Real;
		result.Mnim = x.Mnim - y.Mnim;
		return result;
	}
};

