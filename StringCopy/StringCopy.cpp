#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	char First[] = "Copy me!";
	char Second[10] = "";
	char *x = &First[0];
	char *y = &Second[0];
	for (int i = 0; First[i] != '\0'; i++, *y++ = *x++);
	cout<<Second<<endl;
}