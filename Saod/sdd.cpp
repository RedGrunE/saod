#include "sdd.h";
#include "pch.h";
int* swap_L(int* x, int* y)
{
	int FirstNum = *x;
	for (int*a = x, *n = x + 1; a < y; *a = *n, a++, n++);
	*y = FirstNum;
	return x;
}

int* swap_R(int* x, int* y)
{
	int FirstNum = *y;
	for (int*a = y, *n = y - 1; a > x; *a = *n, a--, n--);
	*x = FirstNum;
	return x;
}