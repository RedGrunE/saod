#include "pch.h"
#include <iostream>
#include <string.h>
using namespace std;

class Woman25
{
private:
	char *name;//имя
	int weight;//вес

	friend void setData(char *, int, Woman25&);//объявление дружественных функций
	friend void getData(Woman25&);
public:
	Woman25()//конструктор
	{
		name = new char[20];
		strcpy(name, "norm");
		weight = 60;
	}
	~Woman25()//деструктор
	{
		delete[] name;
		cout << "!!! DSTRCTR !!!" << endl;
	}

	void setData(char*, int);//объявление методов класса
	void getData();
	void advise();
};

void setData(char *n, int w, Woman25& object)//определяем friend-функцию setData
{
	strcpy(object.name, n);////////////
	object.weight = w;
}

void getData(Woman25& object)//определяем friend-функцию getData
{
	cout << object.name << "\t: " << object.weight << " кг" << endl;
}

void Woman25::setData(char *n, int w)//определяем set-метод класса
{
	strcpy(name, n);
	weight = w;
}

void Woman25::getData()//определяем get-метод класса
{
	cout << name << "\t: " << weight << " кг" << endl;
}

void Woman25::advise()//определяем метод класса Совет (advise)
{
	if (weight < 55) {  //если вес меньше 55 кг
		cout << "Надо больше кушать" << endl;
		cout << "=====================================" << endl << endl;
	}
	else if (weight >= 55 && weight <= 65) {  //если вес в пределах 55-65 кг
		cout << "Ваш вес в норме!" << endl;
		cout << "=====================================" << endl << endl;
	}
	else { //если вес > 65 кг
		cout << "Вам надо ограничивать себя в еде!" << endl;
		cout << "=====================================" << endl << endl;
	}
}

int main()
{
	setlocale(LC_ALL, "rus");

	Woman25 Norm; //создаем объект Norm, сработает конструктор и weight будет = 60, name - Норма
	Norm.getData(); //вызов метода класса
	cout << "=====================================" << endl << endl;

	Woman25 Anna; //второй объект
	Anna.setData('Anna', 100);
	Anna.getData();
	Anna.advise();

	Woman25 Inna; //третий объект
	setData('Inna', 50, Inna);
	getData(Inna);
	Inna.advise();

	return 0;
}